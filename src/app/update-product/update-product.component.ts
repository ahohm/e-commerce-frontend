import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DataService} from "../services/data.service";
import {RestApiService} from "../services/rest-api.service";

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.scss']
})
export class UpdateProductComponent implements OnInit {
  product: any;
  categories: any;
  btnDisabled = false;
  isPromotion = false;

  constructor(private activatedRoute: ActivatedRoute,
              private data: DataService,
              private rest: RestApiService,
              private router: Router) { }

  async ngOnInit() {
    this.activatedRoute.params.subscribe(res => {
      this.rest
        .get(`http://localhost:5000/api/product/${res['id']}`)
        .then(data => {
          data['success']
            ? (this.product = data['product'], this.toPromotion(this.product))
            : this.router.navigate(['/']);
        })
        .catch(error => this.data.error(error['message']));
    });
    try {
      const data = await this.rest.get(
        'http://localhost:5000/api/categories'
      );
      data['success']
        ? (this.categories = data['categories'])
        : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);
    }
  }

  validate(product) {
    if (product.title) {
      if (product.price) {
        if (product.category._id) {
          if (product.description) {

            if (product.stock) {
              if (!this.isPromotion || (this.isPromotion && product.promotion && product.discount)) {
                return true;
              } else if (!product.promotion && !product.discount) {
                this.data.error('Please select Discount percentage and promotion duration');
              }
            } else {
              this.data.error('Please enter number In Stock');
            }

          } else {
            this.data.error('Please enter Description');
          }
        } else {
          this.data.error('Please select Category');
        }
      } else {
        this.data.error('Please enter a Price');
      }
    } else {
      this.data.error('Please enter a Title');
    }
  }

  fileChange(event: any){
    this.product.product_picture = (event.target as HTMLInputElement).files[0];
  }

  async update() {
    this.btnDisabled = true;
    try {
      if (this.validate(this.product)) {
        const form = new FormData();
        for (const key in this.product) {
          if (this.product.hasOwnProperty(key)) {
            if (key === 'product_picture') {
              form.append(
                'product_picture',
                this.product.product_picture,
                this.product.product_picture.name
              );
            } else {
              form.append(key, this.product[key]);
            }
          }
        }
        const data = await this.rest.post(
          `http://localhost:5000/api/seller/products/${this.product._id}`,
          form
        );
        data['success']
          ? this.router.navigate(['/profile/myproducts'])
            .then(()=> this.data.success(data['message']))
            .catch(error => this.data.error(error))
          : this.data.error(data['message']);

      }
    } catch(error){
      this.data.error(error['message']);
    }
    this.btnDisabled = false;
  }

  toPromotion(product) {
    if (product.promotion > 0 && Date.parse(product.finish) > Date.now()) {
      this.isPromotion = true;
    } else {
      product.promotion = 0;
      product.discount = 0;
      this.isPromotion = false;
    }
  }

}
