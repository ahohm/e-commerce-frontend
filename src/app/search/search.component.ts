import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';
import {RestApiService} from '../services/rest-api.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  query: string;
  page = 1;
  result: any;
  content: any;
  constructor(private data: DataService, private rest: RestApiService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(res => {
      this.query = res['query'];
      this.page = 1;
      this.getProducts();
    });
  }

  get lower() {
    return 20 * (this.page - 1) + 1;
  }
  get upper() {
    return  Math.min(this.page * 10, this.result.totalProducts);
  }
  async getProducts() {
    this.content = null;
    try {
      const data = await this.rest.get(`http://localhost:5000/api/search?query=${this.query}&page=${this.page - 1}`);
      data['success'] ? this.result = data : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }
  }

}
