import { Component, OnInit } from '@angular/core';
import {DataService} from "../services/data.service";
import {RestApiService} from "../services/rest-api.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {
  order: any;
  products: any

  constructor(private data: DataService, private rest: RestApiService, private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(res => {
      this.rest
        .get(`http://localhost:5000/api/accounts/orders/${res['id']}`)
        .then(data => {
          data['success']
            ? (this.order = data['order'], this.products = data['order'].products, console.log(this.products))
            : this.router.navigate(['/']);
          console.log(this.order);
        })
        .catch(error => this.data.error(error['message']));
    });
  }
  toState(state) {
    if (state === 0) {
      return 'Waiting';
    } else if (state === 1) {
      return 'Packaging';
    } else if (state === 2) {
      return 'Being sent';
    } else {
      return 'Delivered';
    }
  }
  toDate(date) {
    return new Date(date);
  }

}
