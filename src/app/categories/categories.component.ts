import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../services/rest-api.service'
import { DataService } from '../services/data.service'

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: any
  constructor(
    private data: DataService,
    private rest: RestApiService
  ) { }

  async ngOnInit() {
    try{
      const data = await this.rest.get(
        'http://localhost:5000/api/categories'
      )
      data['success']
      ? (this.categories = data['categories'])
      : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);
    }
  }

}
