import { Router } from '@angular/router';
import { RestApiService } from './../services/rest-api.service';
import { DataService } from './../services/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-product',
  templateUrl: './post-product.component.html',
  styleUrls: ['./post-product.component.scss']
})
export class PostProductComponent implements OnInit {

  product = {
    title: '',
    price: 0,
    categoryId: '',
    description: '',
    product_picture: null,
    promotion : 0,
    discount : 0,
    stock : 1
  };
  categories: any;
  btnDisabled = false;
  isPromotion = false;

  constructor(
    private data: DataService,
    private rest: RestApiService,
    private router: Router
  ) { }

  async ngOnInit(){
    try {
      const data = await this.rest.get(
        'http://localhost:5000/api/categories'
      );
      data['success']
      ? (this.categories = data['categories'])
      : this.data.error(data['message']);
    } catch(error){
      this.data.error(error['message']);
    }
  }
  // chưa cho select product_picture
  validate(product) {
    if (product.title) {
      if (product.price) {
        if (product.categoryId) {
          if (product.description) {
            if (product.product_picture) {
              if (product.stock) {
                if (!this.isPromotion || (this.isPromotion && product.promotion && product.discount)) {
                  return true;
                } else if (!product.promotion && !product.discount) {
                  this.data.error('Please select Discount percentage and promotion duration');
                }
              } else {
                this.data.error('Please enter number In Stock');
              }
            } else {
              this.data.error('Please select Product Image');
            }
          } else {
            this.data.error('Please enter Description');
          }
        } else {
          this.data.error('Please select Category');
        }
      } else {
        this.data.error('Please enter a Price');
      }
    } else {
      this.data.error('Please enter a Title');
    }
  }

  fileChange(event: any){
    this.product.product_picture = (event.target as HTMLInputElement).files[0];
  }

  async post() {
    this.btnDisabled = true;
    try {
      if (this.validate(this.product)) {
        const form = new FormData();
        for (const key in this.product) {
          if (this.product.hasOwnProperty(key)) {
            if (key === 'product_picture') {
              form.append(
                'product_picture',
                this.product.product_picture,
                this.product.product_picture.name
              );
            } else {
              form.append(key, this.product[key]);
            }
          }
        }
        const data = await this.rest.post(
          'http://localhost:5000/api/seller/products',
          form
        );
        data['success']
          ? this.router.navigate(['/profile/myproducts'])
          .then(()=> this.data.success(data['message']))
          .catch(error => this.data.error(error))
          : this.data.error(data['message']);

          }
    } catch(error){
      this.data.error(error['message']);
  }
    this.btnDisabled = false;
}

}
