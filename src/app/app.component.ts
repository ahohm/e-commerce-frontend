import { Component } from '@angular/core';


import { Router } from '@angular/router';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  searchTerm = '';
  isCollapsed = true;

  // public data attribute to escape a common error in angular : modified by Amine
  constructor(private router: Router, public data: DataService) {
    // Added and commented by Amine , It'll be functional after the profile creation
    this.data.getProfile();
    this.data.cartItems = this.data.getCart().length;
  }

  get token() {
    return localStorage.getItem('token');
  }

  collapse() {
    this.isCollapsed = true;
  }

  closeDropdown(dropdown) {
    // dropdown.close();
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/']);
  }

  search() {
    if (this.searchTerm) {
      this.collapse();
      this.router.navigate(['search', {query: this.searchTerm}]);
    }
  }
}
