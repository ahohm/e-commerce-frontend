import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('token')) {
      // allow acces to the /profile route
      return state.url.startsWith('/profile') ? true : (this.router.navigate(['/']), false);
      // this.router.navigate(['/']);
      // return false;
    } else {
      return state.url.startsWith('/profile') ? (this.router.navigate(['/']), true) : true;
    }
  }
}
