import { Component, OnInit } from '@angular/core';
import {DataService} from "../services/data.service";
import {RestApiService} from "../services/rest-api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  orders: any;

  constructor(private data: DataService, private rest: RestApiService, private router: Router) { }

  async ngOnInit() {
    try {
      const data = await this.rest.get(
        'http://localhost:5000/api/accounts/orders'
      );
      data['success']
        ? (this.orders = data['orders'])
        : (this.data.error(data['message']));
    } catch (error) {
      this.data.error(error['message']);
    }
  }
  toDate(date) {
    return new Date(date);
  }
  toState(state) {
    if (state === 0) {
      return 'Waiting';
    } else if (state === 1) {
      return 'Packaging';
    } else if (state === 2) {
      return 'Being sent';
    } else {
      return 'Delivered';
    }
  }

}
