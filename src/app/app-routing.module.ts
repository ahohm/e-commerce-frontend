import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AddressComponent } from './address/address.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { AuthGuardService } from './services/auth-guard.service';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { SearchComponent } from './search/search.component';
import { CategoriesComponent } from './categories/categories.component';
import { PostProductComponent } from './post-product/post-product.component';
import { MyProductsComponent } from './my-products/my-products.component';
import { CategoryComponent } from './category/category.component';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';
import {UpdateProductComponent} from './update-product/update-product.component';
import {OrderComponent} from './order/order.component';
import {OrderDetailComponent} from './order-detail/order-detail.component';


const routes: Routes = [


  { path: '', component: HomeComponent },
  {
    path: 'categories',
    component: CategoriesComponent
  },
  {
    path: 'categories/:id',
    component: CategoryComponent
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'product/:id',
    component: ProductComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path : 'register',
    component : RegistrationComponent,
    canActivate: [AuthGuardService]
  },
  {
    path : 'login',
    component : LoginComponent,
    canActivate: [AuthGuardService]
  },
  {
    path : 'profile',
    component : ProfileComponent,
    canActivate : [AuthGuardService]
  },
  {
    path : 'profile/orders',
    component : OrderComponent,
    canActivate : [AuthGuardService]
  },
  {
    path : 'profile/orders/:id',
    component : OrderDetailComponent,
    canActivate : [AuthGuardService]
  },
  {
    path : 'profile/settings',
    component : SettingsComponent,
    canActivate : [AuthGuardService]
  },
  {
    path : 'profile/address',
    component : AddressComponent,
    canActivate : [AuthGuardService]
  },
  {
    path : 'profile/postproduct',
    component : PostProductComponent,
    canActivate : [AuthGuardService]
  },
  {
    path : 'profile/myproducts',
    component : MyProductsComponent,
    canActivate : [AuthGuardService]
  },
  {
    path : 'profile/myproducts/updateProduct/:id',
    component : UpdateProductComponent,
    canActivate : [AuthGuardService]
  },
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
